<?php

namespace Drupal\findit_migration_utils\Plugin\migrate\process;

use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_get_setting"
 * )
 */
class FindItGetSetting extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $setting_name = $this->configuration['setting'];

    if ($value = Settings::get($setting_name)) {
      return $value;
    }

    return NULL;
  }
}
